USE [onlineQuiz]
GO
/****** Object:  Table [dbo].[history]    Script Date: 5/3/2020 11:01:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[history](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[userID] [int] NOT NULL,
	[point] [float] NOT NULL,
	[date] [date] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Question]    Script Date: 5/3/2020 11:01:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Question](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Content] [nvarchar](max) NULL,
	[answer] [nvarchar](50) NULL,
	[date] [date] NULL,
	[opt1] [nvarchar](max) NULL,
	[opt2] [nvarchar](max) NULL,
	[opt3] [nvarchar](max) NULL,
	[opt4] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[user]    Script Date: 5/3/2020 11:01:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[userName] [nvarchar](50) NOT NULL,
	[passWord] [nvarchar](50) NOT NULL,
	[type] [bit] NOT NULL,
	[email] [nvarchar](50) NULL
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[history] ON 

INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (1, 2, 4, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (2, 2, 4.3, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (3, 3, 4.5659999847412109, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (4, 2, 5, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (5, 2, 3.3333334922790527, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (6, 2, 5, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (7, 2, 2.5, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (8, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (9, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (12, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (13, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (14, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (21, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (22, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (23, 5, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (24, 5, 10, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (25, 5, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (26, 5, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (27, 5, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (28, 5, 7.5, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (29, 5, 10, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (30, 5, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (31, 5, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (36, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (40, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (45, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (46, 0, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (47, 0, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (48, 7, 0, CAST(0xFA400B00 AS Date))
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (10, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (32, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (11, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (15, 7, 5, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (16, 7, 5, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (17, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (18, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (19, 7, 6.6666669845581055, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (20, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (41, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (42, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (43, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (44, 7, 10, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (33, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (34, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (35, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (37, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (38, 7, 0, NULL)
INSERT [dbo].[history] ([id], [userID], [point], [date]) VALUES (39, 7, 0, NULL)
SET IDENTITY_INSERT [dbo].[history] OFF
SET IDENTITY_INSERT [dbo].[Question] ON 

INSERT [dbo].[Question] ([ID], [Content], [answer], [date], [opt1], [opt2], [opt3], [opt4]) VALUES (10, N'2+5 =', N'13', CAST(0xE4400B00 AS Date), N'7', N'2', N'5+2', N'1')
INSERT [dbo].[Question] ([ID], [Content], [answer], [date], [opt1], [opt2], [opt3], [opt4]) VALUES (11, N'2+6 =', N'2', CAST(0xE4400B00 AS Date), N'7', N'8', N'5+2', N'1')
INSERT [dbo].[Question] ([ID], [Content], [answer], [date], [opt1], [opt2], [opt3], [opt4]) VALUES (13, N'bây giờ là?', N'3', CAST(0xE4400B00 AS Date), N'sáng', N'trưa', N'19h', N'tối')
INSERT [dbo].[Question] ([ID], [Content], [answer], [date], [opt1], [opt2], [opt3], [opt4]) VALUES (14, N'how many people in your family?', N'4', CAST(0xE4400B00 AS Date), N'1', N'2', N'3', N'there are four people in my family')
INSERT [dbo].[Question] ([ID], [Content], [answer], [date], [opt1], [opt2], [opt3], [opt4]) VALUES (15, N'3+3=', N'1', CAST(0xE4400B00 AS Date), N'chín', N'chưa chín', N'trưa chín', N'trín ')
INSERT [dbo].[Question] ([ID], [Content], [answer], [date], [opt1], [opt2], [opt3], [opt4]) VALUES (16, N'Mị Châu chọn ai làm chồng?', N'3', CAST(0xE4400B00 AS Date), N'Lạc Long Quân', N'Dương Quá', N'Trọng Thủy', N'Kim Trọng')
SET IDENTITY_INSERT [dbo].[Question] OFF
SET IDENTITY_INSERT [dbo].[user] ON 

INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (1, N'dsad', N'das', 1, N'dasd@dsaa.com')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (2, N'aa', N'vv', 1, N'a@f.c')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (3, N'aa', N'vv', 1, N'a@f.c')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (4, N'tienanh', N'vv', 1, N'a@f.c')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (9, N'tienanh1', N'aaa', 1, N'a@f.g')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (10, N'tienanh1', N'aaa', 1, N'a@f.g')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (11, N'student', N'1', 0, N'dasd@dsaa.com')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (12, N'teacher', N'1', 1, N'dasd@dsaa.com')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (13, N'anh', N'aa', 0, N'dasd@dsaa.com')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (14, N'a1', N'a1', 0, N'dasd@dsaa.com')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (5, N'ttt', N'aaa', 1, N'a@g.f')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (6, N'ttt', N'aaa', 1, N'a@g.f')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (7, N'ttt1', N'aaa', 0, N'a@g.f')
INSERT [dbo].[user] ([ID], [userName], [passWord], [type], [email]) VALUES (8, N'tienanh1', N'aaa', 1, N'a@f.g')
SET IDENTITY_INSERT [dbo].[user] OFF
